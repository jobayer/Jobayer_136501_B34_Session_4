<?php

define("JustAConstant", 'This is a Constant Test');
echo JustAConstant.'<br />';

define("GREETING", "Welcome to Greeting with Constant!", true); //Default false
//constants are automatically global across the entire script.
function globalTest() {
    echo GREETING;
}

globalTest();