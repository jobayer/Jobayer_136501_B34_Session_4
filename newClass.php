<?php

class newClass
{
    public function testMethod(){
        echo 'It Will Print the Current Method name: ';
        echo __METHOD__;  // Output: newClass::testMethod
    }
}

$cls = new newclass();

$cls->testMethod();