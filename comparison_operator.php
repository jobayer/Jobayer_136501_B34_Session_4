<?php
$x = 50;
$y = 50.0;
$z = '50';
$xy = 48;
// Equal oparetor

echo $x == $y ? 'true' : 'false'; //true
echo '<br />';

// Identical oparetor
echo $x === $z ? true : false; //true
echo '<br />';

// Not Equal oparetor
echo $x != $y ? 'true' : 'false'; //false
echo '<br />';

// Less than oparetor
echo $x > $xy ? 'true' : 'false'; //true
echo '<br />';

// Greater than or equal to Oparetor
echo $x >= $xy ? 'true' : 'false'; //true
echo '<br />';

// Spaceship Oparetor
echo $x <=> $xy ? 'true' : 'false'; //true
echo '<br />';
