<?php

    echo "Pre-increment<br />";
    $a = 5;
    print "a= ".++$a."<br />";
    print "a= ".$a."<br />";

    echo "Post-increment<br />";
    $a = 5;
    print "a= ".$a++."<br />";
    print "a= ".$a."<br />";

    echo "Pred-ecrement<br />";
    $a = 5;
    print "a= ".--$a."<br />";
    print "a= ".$a."<br />";

    echo "Post-decrement<br />";
    $a = 5;
    print "a= ".$a--."<br />";
    print "a= ".$a."<br />";