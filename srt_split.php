<?php
$str = "Hello Friend";

$arr1 = str_split($str);
$arr2 = str_split($str, 3);

print_r($arr1); // default: take only 1 char to every array()
print_r($arr2); // take 3 char to every array()