<?php
$str = "A 'quote' is <b>bold</b>";

// Outputs: A 'quote' is &lt;b&gt;bold&lt;/b&gt;
echo htmlentities($str);
echo '<br/>';
// Outputs: A &#039;quote&#039; is &lt;b&gt;bold&lt;/b&gt;
echo htmlentities($str, ENT_QUOTES);
// without htmlentities function the output
echo '<br/>';
echo $str;  // A 'quote' is bold

echo '<br/>';
echo '&lt;br&gt; is HTML TAG'; // Another Way...