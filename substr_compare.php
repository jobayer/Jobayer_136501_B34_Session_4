<?php

echo substr_compare("abcde", "bc", 1, 2); // 0
echo '<br/>';
echo substr_compare("abcde", "de", -2, 2); // 0
echo '<br/>';
echo substr_compare("abcde", "bcg", 1, 2); // 0
echo '<br/>';
echo substr_compare("abcde", "BC", 1, 2, true);   // case-insensitive.. Output: 0
echo '<br/>';
echo substr_compare("abcde", "bc", 1, 4); // 2
echo '<br/>';
echo substr_compare("abcde", "cd", 1, 2); // -1
echo '<br/>';
echo substr_compare("abcde", "abc", 5, 1); // warning
echo '<br/>';

echo substr_compare("Hello world!","Hello world!",0)."<br>"; // the two strings are equal  Output: 0
echo substr_compare("Hello world!","Hello",0)."<br>"; // string1 is greater than string2 Output: 7
echo substr_compare("Hello world!","Hello world! Hello!",0)."<br>"; // string1 is less than string2 Output: -7